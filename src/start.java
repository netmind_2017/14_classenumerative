import java.util.Calendar;
import java.util.Date;

public class start {

	public static void main(String[] args) {
		
		Date currentDate = new Date();	
				
		Day x = getDay(currentDate);
				
		switch (x) {
	        case MONDAY:
	            System.out.println("MONDAY");
	            break;
	        case THURSDAY:
	            System.out.println("THURSDAY.");
	            break;
	        case WEDNESDAY:
	            System.out.println("WEDNESDAY");
	            break;
	        case TUESDAY:
	            System.out.println("TUESDAY");
	            break;	               
	        case FRIDAY:
	            System.out.println("Close to the Weekend !");
	            break;
	        case SATURDAY: case SUNDAY:
	            System.out.println("Weekend !");
	            break;
	        default:
	            System.out.println("ERROR");
	            break;
	    }

	}
	
	@SuppressWarnings({"static-access" })
	public static Day getDay(Date currentDate) {
		
		Calendar c = Calendar.getInstance();
		int dayOfWeek  = c.get(Calendar.DAY_OF_WEEK);
		
		Day x = null;
		
		//Empieza con SUNDAY (Domingo) por que la funci�n calendar formatea 
		//la fecha inglesa (ellos empiezan la semana por domingo)
		if (dayOfWeek == 1)
			x = Day.SUNDAY;
		else if (dayOfWeek == 2)
			x = Day.MONDAY;
		else if (dayOfWeek == 3)
			x = Day.TUESDAY;
		else if (dayOfWeek == 4)
			x = Day.WEDNESDAY;
		else if (dayOfWeek == 5)
			x = Day.THURSDAY;
		else if (dayOfWeek == 6)
			x = Day.FRIDAY;
		else if (dayOfWeek == 7)
			x = Day.SATURDAY;		
		return x;
	}
	
}
